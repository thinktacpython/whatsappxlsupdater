def GetAllSubLeft(path):
    import re
    content=''
    phones=[]
    with open(path,encoding='utf-8') as f:
        content=f.readlines()
    keyword = 'left'
    for line in content:
        r=re.compile('^(\d+\/\d+\/\d+)(,\s)(\d+:\d+\s\w+)(\s-)')
        try:
            ptrn=re.search(r,line).group()
            line=line.replace(ptrn,'')
        except:
            pass
        if keyword in line:
            pNo=[line.partition(keyword)[0]]
            if pNo[0]==' ':
                pNo=pNo[1:]
            if pNo[-1]==' ':
                 pNo=pNo[:-1]
            phones+=pNo
    return phones