def  getidx(path):
    import re
    import pandas as pd
    phones=[]
    dates=[]
    count=[]
    content=''
    chats={}
    date=''
    with open(path,encoding='utf-8') as f:
        content=f.readlines()
    for line in content:
        r=re.compile('^(\d+\/\d+\/\d+)(,\s)(\d+:\d+\s\w+)(\s-)')
        try:
            ptrn=re.search(r,line).group()
            date=ptrn[:-11]
            if len(date)==0:
                continue
            if date[-1]==',':
                date=date[:-1]
            else:
                date=date
            dates+=[date]
            line=line.replace(ptrn,'')
        except:
            continue

        if('Meeting ID') in line:
            continue
        if('Password') in line:
            continue
        if('Topic') in line:
            continue
        if('Time') in line:
            continue
        if not ':' in line:
            continue
        if '/' in line:
            continue
        pPart=[line.partition(':')[0]]
        pParta=pPart[0]+':'
        istoIdx=pParta.find(':')
        pNo=pParta[:istoIdx]
        if 'https:' in line:
            continue
        if pNo[0]==" ":
            pNo=pNo[1:]
        if pNo[-1]==" ":
            pNo=pNo[:-2]
        pNo = pNo.replace(' ', '')
        if '<Media omitted>' in line:
            if not pNo in chats:
                chats[pNo]=1
            else:
                chats[pNo]+=1
        else:
            if not pNo in chats:
                chats[pNo]=1
            else:
                chats[pNo]+=1
    phones=list(chats.keys())
    pdict={i:0 for i in dates}
    dates = list(pdict.keys())
    dates.append("Media")
    dates.append("Text")
    ################################
    df = pd.DataFrame(index=phones,columns=dates)
    df = df.fillna(0)
    return df



def getAllSubActivity(path):
    import re
    import pandas as pd
    df=getidx(path)
    content=''
    chats={}
    date=''
    with open(path,encoding='utf-8') as f:
        content=f.readlines()
    for line in content:
        r=re.compile('^(\d+\/\d+\/\d+)(,\s)(\d+:\d+\s\w+)(\s-)')
        try:
            ptrn=re.search(r,line).group()
            date=ptrn[:-11]
            if date[-1]==',':
                date=date[:-1]
            else:
                date=date
            line=line.replace(ptrn,'')
        except:
            pass
        if('Meeting ID') in line:
            continue
        if('Password') in line:
            continue
        if('Topic') in line:
            continue
        if('Time') in line:
            continue
        if not ':' in line:
            continue
        if '/' in line:
            continue
        pPart=[line.partition(':')[0]]
        pParta=pPart[0]+':'
        istoIdx=pParta.find(':')
        pNo=pParta[:istoIdx]
        if 'https:' in line:
            continue
        if pNo[0]==" ":
            pNo=pNo[1:]
        if pNo[-1]==" ":
            pNo=pNo[:-2]
        pNo = pNo.replace(' ', '')
        try:
            if '<Media omitted>' in line:
                if df.loc[pNo,"Media"]==0:
                    df.loc[pNo,"Media"]=100
                else:
                    df.loc[pNo,"Media"]+=100
            else:
                if df.loc[pNo, "Text"] == 0:
                    df.loc[pNo, "Text"] = 1
                else:
                    df.loc[pNo, "Text"] += 1
                if df.loc[pNo,date]==0:
                    df.loc[pNo,date]=1
                else:
                    df.loc[pNo,date]+=1
        except:
            pass
    return df

