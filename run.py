import pandas as pd
from getallsubactivity import getAllSubActivity
from inoutActivity import inoutActivity
from obwriter import upd
from inOutactivityCount import inoutCount
from details import getDetails
def mainFun(path):
    xlsPath = path.split('.')[0] + '.xlsx'
    sheet1 = getAllSubActivity(path)
    sheet2 = inoutActivity(path)
    detail=getDetails(path)
    df=sheet1
    inoutDict=inoutCount(path)
    upd(sheet1,inoutDict,path,detail)
    sheet1.to_csv(xlsPath+'.csv')
    writer = pd.ExcelWriter(xlsPath, engine='xlsxwriter')

    sheet1.to_excel(writer, sheet_name='Chat_Activity')
    sheet2.to_excel(writer, sheet_name='IN-OUT-activity')

    writer.save()
    return xlsPath
path='/home/salah/Videos/Download/WhatsApp_chat2.txt'
mainFun(path)
