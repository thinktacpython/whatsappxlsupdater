from flask import send_file
import os

tempPath = "tmp"
import run


def cors_enabled_function(request):
    if request.method == 'OPTIONS':
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)

    headers = {
        'Access-Control-Allow-Origin': '*'
    }

    return (analyze(request), 200, headers)


def analyze(request):
    if request.files:
        txtFile = request.files["file"]
        txtFile.save(os.path.join(tempPath, txtFile.filename))
        Xls = run.mainFun(os.path.join(tempPath, txtFile.filename))
        file = open(Xls, "rb")
        return send_file(file, mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    else:
        return ('')
