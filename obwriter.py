import sys, datetime
import firebase_admin as fb
import pandas as pd
from firebase_admin import firestore


# Write scannedobfile to firestore
# Write each row from obanalysisfile into firebase
#   |-schoolid, section
#           |-studentid
#               |-tacid, version
#                   |-obid, col, filledvalue, complete, correct
# 
cred = fb.credentials.Certificate('ServiceAccountKey.json')
default_app = fb.initialize_app(cred)
db = firestore.client()

# Import database module.


# Write analysis results into firestore
def upd(df,inoutD,filename,WAGMessages):
    
    #WAGMessages contains the datastructure for Details in Journey>document>Detais
    rec = df.to_dict(orient='records')
    filename= filename.split('/')[-1].split('.')[0]
    keyz=list(df.index.values)
    keys=[]
    for i in keyz:
        if '+91' in i:
            i=i.replace('+91',"")
            keys.append(i)
        else:
            keys.append(i)
    keydf=pd.DataFrame(keys)
    media=df['Media']
    text=df['Text']
    chat={}
    for i,j,k in zip(keys,media,text):
        chat[i]={"media":j,"text":k}
    dlist=rec[0]
    n = len(rec)
    my_doc_ref = db.collection('whatsappChatFiles').document()
    entry1=dict()
    file ={
        'chatCountData': chat,
        'inoutData': inoutD

    }
    AugmentedData = {
        filename: file,
        'Added_On': firestore.SERVER_TIMESTAMP,
        'testing': bool(True)
    }
    entry1.update(AugmentedData)
    # # st_code = ''
    # if(rec and rec[0] and rec[0]['studentcode']):
    #     st_code = rec[0]['studentcode'].split(".")[0]
    # entry2 = {
    #
    # }

    augmentedKey = {
        "AugmentedData": entry1
    }
   # entry2.update(augmentedKey)
    my_doc_ref.set(entry1, merge=True)
