def  GetAllSubAdded(path):
    import re
    ################
    def findPhoneNo(text):
        p=[]
        for ind,i in enumerate(text):
            if i=='+':
                p.append(text[ind:ind+15])
        return p
    ################
    content=''
    phones=[]
    with open(path,encoding='utf-8') as f:
        content=f.readlines()
    keyword = 'added'
    for line in content:
        r=re.compile('^(\d+\/\d+\/\d+)(,\s)(\d+:\d+\s\w+)(\s-)')
        try:
            ptrn=re.search(r,line).group()
            line=line.replace(ptrn,'')
        except:
            pass
        if keyword in line:
            if '+91' in line:
                no=findPhoneNo(line)
                phones+=[i for i in no]
            else:
                end=re.search(keyword, line).end()
                if end==len(line)-1:
                    continue
                if ':' in line:
                    continue
                line=line.split('\n')[0]
                pNo=[line.partition(keyword)[2]]
                if pNo[0]==' ':
                    pNo=pNo[1:]
                if pNo[-1]==' ':
                    pNo=pNo[:-1]
                phones+=pNo
    pdict={i:0 for i in phones}
    phonez = list(pdict.keys())
    return phonez
#pass a full file path to the function 
# returns a list