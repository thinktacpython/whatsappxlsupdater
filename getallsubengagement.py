# returns two dictionaries
# text and media
# each dictionary is a mapping of phone or name to the contentType



def GetAllSubEngagement(path):
    import re
    import pandas as pd
    text = {}
    media = {}
    content = ''
    phonesText = []
    phonesMedia = []
    with open(path, encoding='utf-8') as f:
        content = f.readlines()
    for line in content:
        r = re.compile('^(\d+\/\d+\/\d+)(,\s)(\d+:\d+\s\w+)(\s-)')
        try:
            ptrn = re.search(r, line).group()
            line = line.replace(ptrn, '')
        except:
            pass
        if ('Meeting ID') in line:
            continue
        if ('Password') in line:
            continue
        if ('Topic') in line:
            continue
        if ('Time') in line:
            continue
        if not ':' in line:
            continue
        pPart = [line.partition(':')[0]]
        pParta = pPart[0] + ':'
        istoIdx = pParta.find(':')
        pNo = pParta[:istoIdx]
        if 'https:' in line:
            continue
        if pNo[0] == " ":
            pNo = pNo[1:]
        if pNo[-1] == " ":
            pNo = pNo[:-2]
        if '<Media omitted>' in line:
            if not pNo in media:
                media[pNo] = 1
            else:
                media[pNo] += 1
        else:
            if not pNo in text:
                text[pNo] = 1
            else:
                text[pNo] += 1
    return text, media
# returns two dictionaries
