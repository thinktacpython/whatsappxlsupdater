def  GetAllUniqueSub(path):
    import re
    ################
    def findPhoneNo(text):
        p=[]
        for ind,i in enumerate(text):
            if i=='+':
                p.append(text[ind:ind+15])
        return p
    ################
    content=''
    phones=[]
    with open(path,encoding='utf-8') as f:
        content=f.readlines()
    keyword0 = 'joined'
    keyword1 = 'Joined'
    keyword2 = 'added'
    for line in content:
        r=re.compile('^(\d+\/\d+\/\d+)(,\s)(\d+:\d+\s\w+)(\s-)')
        try:
            ptrn=re.search(r,line).group()
            line=line.replace(ptrn,'')
        except:
            pass
        if keyword1 in line:
            istoIdx=re.search(":",line).start()
            pNo=[line[:istoIdx]]
            if pNo[0]==' ':
                pNo=pNo[1:]
            if pNo[-1]==' ':
                pNo=pNo[:-1]
            phones+=pNo
        elif keyword0 in line:
            if ':' in line:
                continue
            else:
                pNo=[line.partition(keyword0)[0]]
                if pNo[0]==' ':
                    pNo=pNo[1:]
                if pNo[-1]==' ':
                    pNo=pNo[:-1]
                phones+=pNo
        elif keyword2 in line:
            if '+91' in line:
                no=findPhoneNo(line)
                phones+=[i for i in no]
            else:
                end=re.search(keyword2, line).end()
                if end==len(line)-1:
                    continue
                if ':' in line:
                    continue
                line=line.split('\n')[0]
                pNo=[line.partition(keyword2)[2]]
                if pNo[0]==' ':
                    pNo=pNo[1:]
                if pNo[-1]==' ':
                    pNo=pNo[:-1]
                phones+=pNo
    pdict={i:0 for i in phones}
    phonez = list(pdict.keys())
    return phonez