
def  getidx(path):
    import re
    import pandas as pd
    phones=[]
    dates=[]
    count=[]
    content=''
    chats={}
    date=''
    with open(path,encoding='utf-8') as f:
        content=f.readlines()
    for line in content:
        r=re.compile('^(\d+\/\d+\/\d+)(,\s)(\d+:\d+\s\w+)(\s-)')
        try:
            ptrn=re.search(r,line).group()
            date=ptrn[:-11]
            if len(date)==0:
                continue
            if date[-1]==',':
                date=date[:-1]
            else:
                date=date
            dates+=[date]
            line=line.replace(ptrn,'')
        except:
            continue

        if('Meeting ID') in line:
            continue
        if('Password') in line:
            continue
        if('Topic') in line:
            continue
        if('Time') in line:
            continue
        if not ':' in line:
            continue
        if '/' in line:
            continue
        pPart=[line.partition(':')[0]]
        pParta=pPart[0]+':'
        istoIdx=pParta.find(':')
        pNo=pParta[:istoIdx]
        if 'https:' in line:
            continue
        if pNo[0]==" ":
            pNo=pNo[1:]
        if pNo[-1]==" ":
            pNo=pNo[:-2]
        if '<Media omitted>' in line:
            if not pNo in chats:
                chats[pNo]=1
            else:
                chats[pNo]+=1
        else:
            if not pNo in chats:
                chats[pNo]=1
            else:
                chats[pNo]+=1
    phones=list(chats.keys())
    pdict={i:0 for i in dates}
    dates = list(pdict.keys())
    ################################
    j='joined'
    a='added'
    l='left'
    df = pd.DataFrame(index=[j,a,l],columns=dates)
    df = df.fillna(0)
    return df,j,a,l


def inoutActivity(path):
    from getallsubjoined import GetAllSubJoined
    from getallsubadded import GetAllSubAdded
    from getallsubleft import GetAllSubLeft
    import re
    import pandas as pd
    #df,j,a,l=getidx(path)
    #content=''
    #chats={}
    #date=''
    joined=GetAllSubJoined(path)
    added=GetAllSubAdded(path)
    left=GetAllSubLeft(path)
    members = {}
    for phonenumber in joined:
        members[phonenumber.replace(' ','')] = "joined"
    for phonenumber in added:
        members[phonenumber.replace(' ','')] = "added"
    for phonenumber in left:
        members[phonenumber.replace(' ','')] = "left"
    members1 = {"phone number": list(members.keys()), "status": list(members.values())}
    # print(members1)
    df = pd.DataFrame.from_dict(members1)
    # print(df)
    return df

