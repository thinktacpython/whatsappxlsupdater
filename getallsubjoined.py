def  GetAllSubJoined(path):
    import re
    content=''
    phones=[]
    with open(path,encoding='utf-8') as f:
        content=f.readlines()
    keyword0 = 'joined'
    keyword1 = 'Joined'
    for line in content:
        r=re.compile('^(\d+\/\d+\/\d+)(,\s)(\d+:\d+\s\w+)(\s-)')
        try:
            ptrn=re.search(r,line).group()
            line=line.replace(ptrn,'')
        except:
            pass
        if keyword1 in line:
            istoIdx=re.search(":",line).start()
            pNo=[line[:istoIdx]]
            if pNo[0]==' ':
                pNo=pNo[1:]
            if pNo[-1]==' ':
                pNo=pNo[:-1]
            phones+=pNo
        elif keyword0 in line:
            if ':' in line:
                continue
            else:
                pNo=[line.partition(keyword0)[0]]
                if pNo[0]==' ':
                    pNo=pNo[1:]
                if pNo[-1]==' ':
                    pNo=pNo[:-1]
                phones+=pNo
    pdict={i:0 for i in phones}
    phonez = list(pdict.keys())
    return phonez