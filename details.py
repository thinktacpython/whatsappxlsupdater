import re
import pandas as pd
import pprint
def getDetails(path):
	data={
		"MediaMessages":0,
		"MessageLog":"",
		"ProductID":[],
		"TextMessages":0,
		"email":""
		}
	content=''
	with open(path,encoding='utf-8') as f:
		content=f.readlines()
	for line in content:
		r=re.compile('^(\d+\/\d+\/\d+)(,\s)(\d+:\d+\s\w+)(\s-)')
		try:
			ptrn=re.search(r,line).group()
			line=line.replace(ptrn,'')
		except:
			continue
		if('Meeting ID') in line:
			continue
		if('Password') in line:
			continue
		if('Topic') in line:
			continue
		if('Time') in line:
			continue
		if not ':' in line:
			continue
		if '/' in line:
			continue
		pPart=[line.partition(':')[0]]
		pParta=pPart[0]+':'
		istoIdx=pParta.find(':')
		pNo=pParta[:istoIdx]
		if 'https:' in line:
			continue
		if pNo[0]==" ":
			pNo=pNo[1:]
		if pNo[-1]==" ":
			pNo=pNo[:-2]
		pNo = pNo.replace(' ', '')
		if '+91' in pNo:
			pNo=pNo.replace("+91",'')
		if not pNo in data:
			data[pNo]={
						"MediaMessages":0,
						"MessageLog":[],
						"ProductID":[],
						"TextMessages":0,
						"email":""
						}
		line=line.partition(":")[2]
		if '<Media omitted>' in line:
			data[pNo]["MediaMessages"]+=1
			data[pNo]["MessageLog"].append(line)
		else:
			data[pNo]["TextMessages"]+=1
			data[pNo]["MessageLog"].append(line)
	return data