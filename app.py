import flask
import main

# app = flask.Flask(__name__)
# from flask_cors import CORS, cross_origin
app = flask.Flask(__name__)


# cors = CORS(app)
# app.config['CORS_HEADERS'] = 'Content-Type'
@app.route("/", methods=['POST'])
# @app.route("/")
# @cross_origin()

def index():
    return main.cors_enabled_function(flask.request)
